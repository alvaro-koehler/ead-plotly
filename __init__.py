import pandas as pd
from . import render as rndr

"""
    TODOS 
    - Bar
    - Boxplot
    - StackBar 
"""


def plot(table, line=False, **kwargs):
    """
    Generates a Plot, single line or scattered

    Parameters
    ----------
    table:   pd.Series or pd.Dataframe
        It contains the date to be plotted. It might be:
        - Series: Which will use the index as `y` feature
        - DataFrame: Need to pass both features `x` and `y`

    line:  boolean
        Whether you want scatter plot or line plot
        By default it will take scatter

    Other Parameters
    ----------
    x: str
        Column name to plot in x-axis
    y: str
        Column name to plot in y-axis. Ignored if Series as data
    hue: str
        Column on which the data will be categorized (a color per group)
    title: str
        Title to be drawn at the plot
    key_name: str or list(str)
        Whether highlight an(y) item(s)
        Handled by set_colors
    xlabel: str
        X-axis label
    ylabel: str
        Y-axis label
    xaxis: dict
        X Axes, inherited from Plotly
        More info at https://plot.ly/python/axes/
    yaxis: dict
        X Axes, inherited from Plotly
        More info at https://plot.ly/python/axes/
    name: str
        Name of the file to store
        By default `plot.png`
    output_dir: str
        Path to store the results at
        By default `figures`
    show: boolean
        Whether show the plot in the notebook
        By default True
    width: int
        Width of the plot
        By default 1000
    height: int
        Height of the plot
        By default 800

    Raises
    -------
    KeyError: If x or y are not in the data set

    Returns
    -------
    A `string` with the location of the file

    """
    try:
        # Check whether this is a valid request
        assert not isinstance(table, pd.Series) and not 'hue' in kwargs, \
            'You can not categorize a Series, remove "hue" from the params'

        # Impute values
        s_names = None
        s_values = None
        x = None
        y = None

        # Option 1:
        # We are plotting a Series
        # -----------------------------------
        if isinstance(table, pd.Series):
            s_names = table.index
            s_values = table.values
        # Option 2:
        # We are plotting a DataFrame
        # -----------------------------------
        elif isinstance(table, pd.DataFrame):
            x = kwargs['x']
            y = kwargs['y']
            s_names = table[x]
            s_values = table[y]

        # Build the data structure to plot
        data = []

        # Do we want to plot by categories (hue) ?
        # Otherwise build simple scatter
        # -----------------------------------
        if 'hue' in kwargs:
            # Extract values and get a color per category
            hue = kwargs['hue']
            categories = table[hue].unique()
            num_cats = len(categories)
            colors = rndr.get_hue_colors(num_cats)

            # Build the ZIP of categories, colors
            # Iterate over the dataset and generate a plotly data trace
            cat_color = zip(categories, colors)
            for category, color in cat_color:
                # Get the data frame
                struct = table.query(f'"{hue}" == "{category}"')
                x_vals = struct[x]
                y_vals = struct[y]
                # Build the trace and append
                trace = rndr.build_scatter_trace(x_vals, y_vals, category,
                                                 color)
                data.append(trace)
        else:
            name = ''
            if 'name' in kwargs:
                name = kwargs['name']
            trace = rndr.build_scatter_trace(s_names, s_values, name, line)
            data = [trace]

        return rndr.plotter(data, **kwargs)

    except KeyError as ke:
        print('At least one of the required parameters is missing or '
              'does not exist in the current table')
        raise ke
    except AssertionError as ass:
        raise ass


def piechart(table, **kwargs):
    """
    Plots a piechart

    Parameters
    ----------
    table:   pd.Series
        It contains the date to be plotted

    Other Parameters
    ----------
    title: str
        Title to be drawn at the plot

    name: str
        Name of the file to store
        By default `plot.png`
    output_dir: str
        Path to store the results at
        By default `figures`
    show: boolean
        Whether show the plot in the notebook
        By default True
    width: int
        Width of the plot
        By default 1000
    height: int
        Height of the plot
        By default 800

    Raises
    -------
    KeyError: If x or y are not in the data set
    AssertionError: If you pass another object except a  Series

    Returns
    -------
    A `string` with the location of the file

    """
    try:
        assert isinstance(table, pd.Series), 'We require a Pandas Series'

        s_names = table.index
        s_values = table.values

        # Build the data structure to plot
        plot_type = 'pie'
        data = [{
            'type': plot_type,
            'labels': s_names,
            'values': s_values,
        }]
        return rndr.plotter(data, **kwargs)

    except KeyError as ke:
        print('At least one of the required parameters is missing or '
              'does not exist in the current table')
        raise (ke)
    except AssertionError as ass:
        raise (ass)


# Key feature (element o list of elements)
# stacked_by

def barchart(table, **kwargs):
    """
       Creates a Bar Chart Plot

       Parameters
       ----------
       table:   pd.Series or pd.Dataframe
           It contains the date to be plotted. It might be:
           - Series: Which will use the index as `y` feature
           - DataFrame: Need to pass both features `x` and `y`

       Other Parameters
       ----------
       x: str
           Column name to plot in x-axis. Ignored if Series as data
       y: str
           Column name to plot in y-axis. Ignored if Series as data
       hue: str
           Column on which the data will be categorized (a color per group)
       title: str
           Title to be drawn at the plot
       key_name: str or list(str)
           Whether highlight an(y) item(s)
           Handled by set_colors
       xlabel: str
           X-axis label
       ylabel: str
           Y-axis label
       xaxis: dict
           X Axes, inherited from Plotly
           More info at https://plot.ly/python/axes/
       yaxis: dict
           X Axes, inherited from Plotly
           More info at https://plot.ly/python/axes/
       name: str
           Name of the file to store
           By default `plot.png`
       output_dir: str
           Path to store the results at
           By default `figures`
       show: boolean
           Whether show the plot in the notebook
           By default True
       width: int
           Width of the plot
           By default 1000
       height: int
           Height of the plot
           By default 800

       Raises
       -------
       KeyError: If x or y are not in the data set

       Returns
       -------
       A `string` with the location of the file

       """
    try:
        # Check whether this is a valid request
        assert not isinstance(table, pd.Series) and not 'hue' in kwargs, \
            'You can not categorize a Series, remove "hue" from the params'

        # Impute values
        s_names = None
        s_values = None
        x = None
        y = None

        # Option 1:
        # We are plotting a Series
        # -----------------------------------
        if isinstance(table, pd.Series):
            s_names = table.index
            s_values = table.values
        # Option 2:
        # We are plotting a DataFrame
        # -----------------------------------
        elif isinstance(table, pd.DataFrame):
            x = kwargs['x']
            y = kwargs['y']
            s_names = table[x]
            s_values = table[y]

        # Build the data structure to plot
        data = []

        # Do we want to plot by categories (hue) ?
        # Otherwise build simple scatter
        # -----------------------------------
        if 'hue' in kwargs:
            pass
        else:
            pass

        return rndr.plotter(data, **kwargs)

    except KeyError as ke:
        print('At least one of the required parameters is missing or '
              'does not exist in the current table')
        raise ke
    except AssertionError as ass:
        raise ass
