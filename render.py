import subprocess
import os
import numpy as np
from .config import BASE_COLOR, HIGHLIGHT_COLOR, COLORS, DOT_SIZE
from .config import HEIGHT, WIDTH, OUTPUT_DIR, PLOT_DEFAULT_NAME
import plotly.graph_objs as go
from plotly import offline


def set_axis(**kwargs):
    xaxis, xlabel, yaxis, ylabel = None, None, None, None

    # Retrieve kwargs elements
    if 'xaxis' in kwargs:
        xaxis = kwargs['xaxis']
    if 'yaxis' in kwargs:
        xaxis = kwargs['yaxis']
    if 'xlabel' in kwargs:
        xlabel = kwargs['xlabel']
    if 'ylabel' in kwargs:
        ylabel = kwargs['ylabel']

    # Build axis
    if not xaxis:
        xaxis = dict(title=xlabel)
    if not yaxis:
        yaxis = dict(title=ylabel, showgrid=False)
    return xaxis, yaxis


def set_colors(key_name, s_names):
    """
    key_name: str or list of str
    """
    if key_name:
        key_name = key_name if isinstance(key_name, list) else [
            key_name]  # to allow you to give 1 or multiple key name
        assert all([True if x in s_names else False for x in key_name])
        condlist = [~s_names.isin(key_name)]
        choicelist = [BASE_COLOR]
        return np.select(condlist, choicelist, default=HIGHLIGHT_COLOR)
    else:
        return HIGHLIGHT_COLOR


def get_hue_colors(n_values):
    """
    Parameters:
    ----------
    n_values: number of colors needed
    return a dict of categorical colors
    """
    color_error = 'There are more categories than colors, ' \
                  'please extend `config.COLORS`'
    assert len(COLORS) >= n_values, color_error
    return COLORS[:n_values]


def plotter(data, **kwargs):
    """
    Function which takes a plotly data structure and several metadata for
    building the Layout

    Other Parameters
    ----------
    title: str
        Title to be drawn at the plot
    xlabel: str
        X-axis label
    ylabel: str
        Y-axis label
    xaxis: dict
        X Axes, inherited from Plotly
        More info at https://plot.ly/python/axes/
    yaxis: dict
        X Axes, inherited from Plotly
        More info at https://plot.ly/python/axes/
    name: str
        Name of the file to store
        By default `plot.png`
    output_dir: str
        Path to store the results at
        By default `figures`
    show: boolean
        Whether show the plot in the notebook
        By default True
    width: int
        Width of the plot
        By default 1000
    height: int
        Height of the plot
        By default 800

    """

    # Build the base layout
    layout = None
    # If I already build a layout, means that it might be a "special" axes
    # i.e. scatter plot with fitting line, in such case inherit this one
    if 'layout' in kwargs:
        layout = kwargs['layout']
    else:
        xaxis, yaxis = set_axis(**kwargs)
        layout = go.Layout(xaxis=xaxis, yaxis=yaxis)

    # Copy elements from kwargs (if any)
    if 'title' in kwargs:
        layout['title'] = kwargs['title']

    if 'width' in kwargs:
        layout['width'] = kwargs['width']
    else:
        layout['width'] = WIDTH

    if 'height' in kwargs:
        layout['height'] = kwargs['height']
    else:
        layout['height'] = HEIGHT

    show = True
    if 'show' in kwargs:
        show = kwargs['show']

    output_dir = OUTPUT_DIR
    if 'output_dir' in kwargs:
        output_dir = kwargs['output_dir']

    name = PLOT_DEFAULT_NAME
    if 'output_dir' in kwargs:
        output_dir = kwargs['output_dir']

    # Plot the data
    fig = go.Figure(data=data, layout=layout)
    file_path = save_plot(fig, output_dir, name)
    show_plot(fig, show)
    return file_path


# Building the plots
# -----------------------------------
def build_scatter_trace(x_val, y_val, plot_name, line, m_color=BASE_COLOR):
    """
    Used for scatter plots
    Given two arrays, color config and plot name, returns a Trace as input
    for data vector.

    Parameters
    ----------
    x_val:  list
        Vector 1
    y_val:  list
        Vector2
    m_color:    str
        RGB Color of the set
    plot_name:  str
        Name of the plot
    line: boolean
        True if you want line
        False if you want dots

    Returns
    -------
    Trace item. It will be the input of `data` in Plotly


    """

    aspect = 'line' if line else 'markers'
    trace = go.Scatter(
        x=x_val,
        y=y_val,
        mode=aspect,
        marker=go.Marker(color=m_color, size=DOT_SIZE),
        name=plot_name
    )

    return trace


# Managing Plots
# -----------------------------------
def html_to_png(html_path, png_path):
    option = {
        "format": "png",
        "xvfb": "",
        "quality": 100,
        "quiet": ''
    }
    subprocess.call(
        [f'xvfb-run wkhtmltoimage {html_path} {png_path}'],
        shell=True
    )


def save_plot(fig, output_dir, name):
    """
    Save a plotly figure localy

    Parameters;
    -------------
    fig: plotly fig
        Figure to be savec
    output_dir: str
        Path to the folder
    name: str
        Name of the plot (without extension)
        If name is None, the fig is not saved

    Returns:
    --------
    Path of the saved figure
    """

    if name is None:
        return None
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    # save to html and then convert to png
    file_path = os.path.join(output_dir, name)
    html_file_path = f"{file_path}.html"
    png_file_path = f"{file_path}.png"

    offline.plot(fig, filename=file_path, show_link=False)
    html_to_png(html_file_path, png_file_path)
    return png_file_path


def show_plot(fig, show=False):
    """
        Display a plotly figure on a current notebook
        Parameters:
        -----------
        fig: Plotly fig
        shwo: bool
            True is you want to display the plot or not
    """
    if show:
        offline.init_notebook_mode(connected=False)
        offline.iplot(fig, show_link=False)
