import os
import pandas as pd
import subprocess
import plotly.tools as tls
import plotly.offline as offline
import plotly.graph_objs as go
import matplotlib.pyplot as plt



# tls.set_credentials_file(username='pmidatascience',
#                         api_key='u8pJHwHg67PEZPkWvcR8')

def format_plot(figsize=(16, 9)):
    """
    Define a standard format for a plot and create a new figure with the given figsize

    Parameters:
    -----------
    figsize: tuple
        size of the figure

    """

    plt.rcParams['font.size'] = 16
    plt.rcParams['axes.titlesize'] = 24
    plt.style.use(
        'seaborn-white')  # https://tonysyu.github.io/raw_content/matplotlib-style-gallery/gallery.html
    plt.figure(figsize=figsize)


def add_legend():
    plt.legend(loc='best')


def annotate_plot(text, list_number=0, space_below_graph=100):
    """
    add annotation below a figure

    Parameters:
    -----------
    test: str
        text to be print below the graph
    list_number: int
        in case of multiple annotation to be print to below each other, increase the list_number. 1st elem -> 0, 2nd ->1m 3td ->2, ...
    space_below_graph: int
        space beteen the x-axis and the first annotation. [in pixel]
    """
    plt.annotate(text, xy=(0, -space_below_graph - list_number * 25),
                 xycoords='axes pixels', size=16)


def save_plot(name, output_dir):
    """
    Save a plot in png format

    Parameters:
    -----------
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved
    """
    file_path = os.path.join(output_dir, name)
    plt.savefig(file_path, format='png', bbox_inches="tight")
    return file_path



def ploty_bar_chart(series, title, xlab="", ylab="", key_name="", name="plot",
                    output_dir="figures", show=False, xaxis=None, yaxis=None,
                    width=700, height=450):
    """
    Save a bar plot in png format using plotly module

    Parameters:
    -----------
    series: pandas series
        series value to plot
    title: str
        title of the plot
    xlab: str
        label for the x axis
    ylab: str
        label for the y axis
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved
    """

    assert isinstance(series, pd.Series), "series is not a pandas serie"

    s_names = list(series.index)
    s_values = list(series)
    nval = len(s_values)

    assert nval > 0

    key_color = 'rgb(0,191,255)'
    other_color = 'rgba(58, 71, 80, 0.6)'  # dark_grey

    key_name = "putain"
    if key_name != "":
        #assert key_name in s_names
        i_key = [i for i in range(nval) if s_names[i] == key_name][0]
        rgb_colors = [other_color for i in range(nval)]
        rgb_colors[i_key] = key_color
    else:
        rgb_colors = 'rgb(0,191,255)'

    data = [{'type': 'bar',
             'marker': go.Marker(color=rgb_colors),
             'x': s_names,
             'y': s_values
             }]

    if not xaxis:
        xaxis = dict(title=xlab)
    if not yaxis:
        yaxis = dict(title=ylab, showgrid=False)

    layout = go.Layout(title=title,
                       width=width,
                       height=height,
                       xaxis=xaxis,
                       yaxis=yaxis)

    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path


def ploty_stack_bar_chart(df, x_feature="Country", y_feature="RatioExp",
                          by_feature="NdaysBucket",
                          title="", xlab="", ylab="", name="plot",
                          output_dir="figures", show=False):
    """
    Save a stacked bar plot in png format using plotly module

    Parameters:
    -----------
    df: pandas dataframe
        df value to plot
    title: str
        title of the plot
    xlab: str
        label for the x axis
    ylab: str
        label for the y axis
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved
    """

    assert isinstance(df, pd.DataFrame), "df is not a pandas DataFrame"
    assert len(df) > 0, "Dataframe with zero records"
    cols = df.columns

    col2have = [x_feature, y_feature, by_feature]
    assert all([True if x in cols else False for x in col2have])

    colors = ["light_grey", "dark_grey", "lightskyblue", "deepskyblue", "blue",
              "blueviolet", "indigo"]

    color_dic = {"light_grey": 'rgb(204,204,204)',
                 "dark_grey": 'rgba(58, 71, 80, 0.6)',
                 "lightskyblue": 'rgb(135,206,250)',
                 "deepskyblue": 'rgb(0,191,255)',
                 "blue": 'rgb(49,130,189)',
                 "blueviolet": 'rgb(138,43,226)',
                 "indigo": 'rgb(75,0,130)'}

    nval = len(df[by_feature].unique())
    assert nval <= len(colors)

    """
    data =[{
        'type': 'bar',
        'x': list(df[df[by_feature] == i][x_feature]),
        'y': list(df[df[by_feature] == i][y_feature]),
        'text':[str(round(x, 2)) + "%" for x in df[df[by_feature] == i][y_feature] ], # add labels
        'textposition': 'auto',
        'name': i}  for i in df[by_feature].unique()]
    """

    data = [{
        'type': 'bar',
        'x': list(df[df[by_feature] == df[by_feature].unique()[i]][x_feature]),
        'y': list(df[df[by_feature] == df[by_feature].unique()[i]][y_feature]),
        'text': [str(round(x, 2)) + "%" for x in
                 df[df[by_feature] == df[by_feature].unique()[i]][y_feature]],
        # add labels
        'textposition': 'auto',
        'marker': go.Marker(color=color_dic[colors[i]]),
        'name': df[by_feature].unique()[i]} for i in range(nval)]

    layout = go.Layout(title=title,
                       xaxis=dict(title=xlab, showgrid=False),
                       yaxis=dict(title=ylab, showgrid=False),
                       # width=800, height=640,
                       barmode='stack')

    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path


def plotly_stack_bar_chart2(df, x_feature="Country", y_feature="RatioExp",
                            by_feature="NdaysBucket", key_feature="",
                            title="", xlab="", ylab="", name="plot",
                            output_dir="figures", show=False):
    """
    Save a stacked bar plot in png format using plotly module

    Parameters:
    -----------
    df: pandas dataframe
        df value to plot
    title: str
        title of the plot
    xlab: str
        label for the x axis
    ylab: str
        label for the y axis
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved
    """

    assert isinstance(df, pd.DataFrame), "df is not a pandas DataFrame"
    assert len(df) > 0, "Dataframe with zero records"
    cols = df.columns

    col2have = [x_feature, y_feature, by_feature]
    assert all([True if x in cols else False for x in col2have])

    feature_list = df[by_feature].unique()
    nval = len(feature_list)

    key_color = 'rgb(0,191,255)'
    min_color = 130
    max_color = 220
    delta_color = int(round((max_color - min_color) / (nval - 1)))

    if key_feature != "":
        assert key_feature in feature_list

        i_key = [i for i in range(nval) if feature_list[i] == key_feature][0]
        data = [{
            'type': 'bar',
            'x': list(df[df[by_feature] == df[by_feature].unique()[i_key]][
                          x_feature]),
            'y': list(df[df[by_feature] == df[by_feature].unique()[i_key]][
                          y_feature]),
            'text': [str(int(round(x))) + "%" for x in
                     df[df[by_feature] == df[by_feature].unique()[i_key]][
                         y_feature]],  # add labels
            'textposition': "auto",
            'marker': go.Marker(color=key_color),
            'name': str(feature_list[i_key]) if str(
                feature_list[i_key]).isupper() else str(
                feature_list[i_key]).replace("_", " ").title()}]
    else:
        data = []
        i_key = -1

    data = data + [{
        'type': 'bar',
        'x': list(df[df[by_feature] == df[by_feature].unique()[i]][x_feature]),
        'y': list(df[df[by_feature] == df[by_feature].unique()[i]][y_feature]),
        'text': [str(int(round(x))) + "%" for x in
                 df[df[by_feature] == df[by_feature].unique()[i]][y_feature]],
        # add labels
        'textposition': "auto",
        'marker': go.Marker(
            color='rgb(' + str(min_color + i * delta_color) + "," + str(
                min_color + i * delta_color) + "," + str(
                min_color + i * delta_color) + ")"),
        'name': str(feature_list[i]) if str(feature_list[i]).isupper() else
        str(feature_list[i]).replace("_", " ").title()}
        for i in range(nval) if i != i_key]

    layout = go.Layout(title=title,
                       xaxis=dict(title=xlab, showgrid=False),
                       yaxis=dict(title=ylab, showgrid=False),
                       # width=800, height=640,
                       barmode='stack')

    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path


def ploty_grouped_bar_chart(df, x_feature="Country", y_feature="RatioExp",
                            by_feature="NdaysBucket", show=False,
                            key_feature="",
                            title="", xlab="", ylab="", name="plot",
                            output_dir="figures", width=700, height=450,
                            xaxis=None):
    """
    Save a group bar plot in png format using plotly module

    Parameters:
    -----------
    df: pandas dataframe
        df with value to plot
    title: str
        title of the plot
    xlab: str
        label for the x axis
    ylab: str
        label for the y axis
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved
    """

    assert isinstance(df, pd.DataFrame), "df is not a pandas DataFrame"
    assert len(df) > 0, "Dataframe with zero records"
    cols = df.columns

    col2have = [x_feature, y_feature, by_feature]
    assert all([True if x in cols else False for x in col2have])
    nval = df[by_feature].nunique()

    key_color = 'rgb(0,191,255)'
    min_color = 130
    max_color = 220
    delta_color = int(round((max_color - min_color) / (nval - 1)))

    feature_list = df[by_feature].unique()
    if key_feature != "":
        i_key = [i for i in range(nval) if feature_list[i] == key_feature][0]
        data = [{
            'type': 'bar',
            'x': list(df[df[by_feature] == df[by_feature].unique()[i_key]][
                          x_feature]),
            'y': list(df[df[by_feature] == df[by_feature].unique()[i_key]][
                          y_feature]),
            'text': [str(int(round(x))) + "%" for x in
                     df[df[by_feature] == df[by_feature].unique()[i_key]][
                         y_feature]],  # add labels
            'textposition': "auto",
            'marker': go.Marker(color=key_color),
            'name': str(feature_list[i_key]) if str(
                feature_list[i_key]).isupper() else str(
                feature_list[i_key]).replace("_", " ").title()}]
    else:
        data = []
        i_key = -1

    data = data + [{
        'type': 'bar',
        'x': list(df[df[by_feature] == df[by_feature].unique()[i]][x_feature]),
        'y': list(df[df[by_feature] == df[by_feature].unique()[i]][y_feature]),
        'text': [str(int(round(x))) + "%" for x in
                 df[df[by_feature] == df[by_feature].unique()[i]][y_feature]],
        # add labels
        'textposition': "auto",
        'marker': go.Marker(
            color='rgb(' + str(min_color + i * delta_color) + "," + str(
                min_color + i * delta_color) + "," + str(
                min_color + i * delta_color) + ")"),
        'name': str(feature_list[i]) if str(feature_list[i]).isupper() else
        str(feature_list[i]).replace("_", " ").title()}
        for i in range(nval) if i != i_key]
    if not xaxis:
        xaxis = dict(title=xlab)

    layout = go.Layout(title=title,
                       xaxis=xaxis,  # autorange=False
                       yaxis=dict(title=ylab, showgrid=False),
                       width=width, height=height,
                       barmode='group')

    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path


def ploty_grouped_bar_chart_no_txt(df, x_feature="Country",
                                   y_feature="RatioExp",
                                   by_feature="NdaysBucket", show=False,
                                   key_feature="",
                                   title="", xlab="", ylab="", name="plot",
                                   output_dir="figures"):
    """
    Save a group bar plot in png format using plotly module

    Parameters:
    -----------
    df: pandas dataframe
        df with value to plot
    title: str
        title of the plot
    xlab: str
        label for the x axis
    ylab: str
        label for the y axis
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved
    """

    assert isinstance(df, pd.DataFrame), "df is not a pandas DataFrame"
    assert len(df) > 0, "Dataframe with zero records"
    cols = df.columns

    col2have = [x_feature, y_feature, by_feature]
    assert all([True if x in cols else False for x in col2have])
    nval = df[by_feature].nunique()

    colors = ["light_grey", "dark_grey", "lightskyblue", "deepskyblue", "blue",
              "blueviolet", "indigo"]

    color_dic = {"light_grey": 'rgb(204,204,204)',
                 "dark_grey": 'rgba(58, 71, 80, 0.6)',
                 "lightskyblue": 'rgb(135,206,250)',
                 "deepskyblue": 'rgb(0,191,255)',
                 "blue": 'rgb(49,130,189)',
                 "blueviolet": 'rgb(138,43,226)',
                 "indigo": 'rgb(75,0,130)'}

    assert nval <= len(colors)

    data = [{
        'type': 'bar',
        'x': list(df[df[by_feature] == df[by_feature].unique()[i]][x_feature]),
        'y': list(df[df[by_feature] == df[by_feature].unique()[i]][y_feature]),
        # 'text':[str(round(x, 2)) + "%" for x in df[df[by_feature] == df[by_feature].unique()[i]][y_feature] ], # add labels
        # 'text':[round(x, 0) for x in df[df[by_feature] == df[by_feature].unique()[i]][y_feature] ], # add labels
        'textposition': 'auto',
        'marker': go.Marker(color=color_dic[colors[i]]),
        # 'marker': go.Marker(color = i, colorscale='Viridis'),
        'name': df[by_feature].unique()[i]} for i in range(nval)]

    layout = go.Layout(title=title,
                       xaxis=dict(title=xlab),
                       yaxis=dict(title=ylab),
                       # width=800, height=640,
                       barmode='group')

    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path

def ploty_bar_chart_overlay(df, x_feature="Country", freq_feature="RatioExp",
                            by_feature="NdaysBucket", order_feature="",
                            title="", xlab="", ylab="", name="plot",
                            output_dir="figures", show=False, bw=False,
                            width=1000, height=600):
    features = list(df[by_feature].unique())
    if (order_feature != ""):
        features.sort(
            key=lambda x: sum(df[df[by_feature] == x][order_feature]),
            reverse=False)
    nval = len(features)
    min_color = 130
    max_color = 220
    delta_color = int(round((max_color - min_color) / (nval - 1)))

    data = []
    if bw == True:

        for i in range(nval):
            data = data + [{
                'type': 'bar',
                'x': df[df[by_feature] == features[i]][x_feature],
                'y': df[df[by_feature] == features[i]][freq_feature],
                'marker': go.Marker(color='rgb(' + str(
                    min_color + i * delta_color) + "," + str(
                    min_color + i * delta_color) + "," + str(
                    min_color + i * delta_color) + ")",
                                    opacity=round(1 - i / (3 * nval), 2)),
                'name': str(features[i])
            }]

    else:
        for i in range(nval):
            data = data + [{
                'type': 'bar',
                'x': df[df[by_feature] == features[i]][x_feature],
                'y': df[df[by_feature] == features[i]][freq_feature],
                'name': str(features[i]),
                'marker': go.Marker(
                    opacity=0.5)
            }]

    layout = go.Layout(barmode='overlay',
                       width=width,
                       height=height,
                       xaxis=dict(
                           autotick=False,
                           ticks='outside',
                           tickcolor='#000',
                           title=xlab
                       ),
                       title=title,
                       yaxis=dict(showgrid=False)
                       )
    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path



def ploty_boxplot(df, y_feature="ExperienceDuration", by_feature="EndReason",
                  title="", ylab="", name="plot", output_dir="figures",
                  show=False):
    assert isinstance(df, pd.DataFrame), "df is not a pandas DataFrame"
    assert len(df) > 0, "Dataframe with zero records"

    cols = df.columns
    col2have = [y_feature, by_feature]
    assert all([True if x in cols else False for x in col2have])

    colors = ["light_grey", "dark_grey", "lightskyblue", "deepskyblue", "blue",
              "blueviolet", "indigo"]

    color_dic = {"light_grey": 'rgb(204,204,204)',
                 "dark_grey": 'rgba(58, 71, 80, 0.6)',
                 "lightskyblue": 'rgb(135,206,250)',
                 "deepskyblue": 'rgb(0,191,255)',
                 "blue": 'rgb(49,130,189)',
                 "blueviolet": 'rgb(138,43,226)',
                 "indigo": 'rgb(75,0,130)'}

    # 'marker': go.Marker(color = color_dic[colors[i]]),
    nval = df[by_feature].nunique()
    assert nval <= len(colors)

    data = [{
        'type': 'box',
        'y': list(df[df[by_feature] == df[by_feature].unique()[i]][y_feature]),
        'marker': go.Marker(color=color_dic[colors[i]]),
        'name': df[by_feature].unique()[i]} for i in range(nval)]

    """
    data =[{
        'type': 'box',
        'y': list(df[df[by_feature] == i][y_feature]),
        'name': i}  for i in df[by_feature].unique()]
    """

    layout = go.Layout(title=title,
                       xaxis=dict(title=""),
                       yaxis=dict(title=ylab))

    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path


def ploty_grouped_boxplot(df, x_feature="Country",
                          y_feature="ExperienceDuration",
                          by_feature="EndReason",
                          title="", xlab="", ylab="", name="plot",
                          output_dir="figures", show=False):
    assert isinstance(df, pd.DataFrame), "df is not a pandas DataFrame"
    assert len(df) > 0, "Dataframe with zero records"

    cols = df.columns
    col2have = [x_feature, y_feature, by_feature]
    assert all([True if x in cols else False for x in col2have])

    colors = ["light_grey", "dark_grey", "lightskyblue", "deepskyblue", "blue",
              "blueviolet", "indigo"]

    color_dic = {"light_grey": 'rgb(204,204,204)',
                 "dark_grey": 'rgba(58, 71, 80, 0.6)',
                 "lightskyblue": 'rgb(135,206,250)',
                 "deepskyblue": 'rgb(0,191,255)',
                 "blue": 'rgb(49,130,189)',
                 "blueviolet": 'rgb(138,43,226)',
                 "indigo": 'rgb(75,0,130)'}

    # 'marker': go.Marker(color = color_dic[colors[i]]),
    nval = df[by_feature].nunique()
    assert nval <= len(colors)

    data = [{
        'type': 'box',
        'x': list(df[df[by_feature] == df[by_feature].unique()[i]][x_feature]),
        'y': list(df[df[by_feature] == df[by_feature].unique()[i]][y_feature]),
        'marker': go.Marker(color=color_dic[colors[i]]),
        'name': df[by_feature].unique()[i]} for i in range(nval)]

    layout = go.Layout(title=title,
                       xaxis=dict(title=xlab),
                       yaxis=dict(title=ylab),
                       boxmode='group')

    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path


def get_colors(size):
    key_color = 'rgb(0,191,255)'
    min_color = 130
    max_color = 220
    delta_color = int(round((max_color - min_color) / (size - 1)))
    color_list = [key_color]
    rgb_list = [min_color + i * delta_color for i in range(size - 1)]
    for elem in rgb_list:
        new_color = 'rgb({0},{0},{0})'.format(elem)
        color_list.append(new_color)
    return color_list



def get_categorical_colors(n_values):
    """
    Parameters:
    ----------
    n_values: number of colors needed
    return a dict of categorical colors
    """
    colors = [
        'rgb(0,191,255)',  # "deepskyblue"
        'rgb(169, 169, 169)',  # "dark_grey"
        'rgb(75,0,130)',  # "indigo"
        'rgb(255, 195, 0)',  # "orange"
    ]
    assert len(
        colors) >= n_values, "there are not enough colors to represent all categories"
    return colors[:n_values]


def get_common_range(list_fig):
    """
    Given a list of plotlyt figs, it return the greatest common xrange and yrange
    Parameters:
    ----------
    list_fig: list of plotly fig

    Return:
        xrange, yrange
    """
    y_minimum = min([min(data.y) for fig in list_fig for data in fig.data])
    y_maximum = max([max(data.y) for fig in list_fig for data in fig.data])
    x_minimum = min([min(data.x) for fig in list_fig for data in fig.data])
    x_maximum = max([max(data.x) for fig in list_fig for data in fig.data])
    return (x_minimum, x_maximum), (y_minimum, y_maximum)


def combine_plots(list_of_figs, name=None, output_dir='reports/figures/',
                  show=False, rows=1, cols=2, width=1200, height=500,
                  same_x_range=False, same_y_range=False,
                  show_legend=[True, False]):
    """
    Given a list of plotly figs, it combine them into subplots

    Parameters:
    -----------
    list_of_figs: list of plotly fig
        fig to be combined all together
    name: str
        name of the file to be saved (if none, not savec)
    output_dir: str
        output directory
    show: Bool
        disply the plot of true
    rows: int
        number of rows in the subplot
    cols:  int
        number of columns in the subplot
    width: int
        width of the subplots
    height: int
        height of the subplot
    same_x_range: bool
        if True, the x range will be the same for all plots
    same_y_range: bool
        if True, the y range will be the same for all plots
    show_legend: list of bool
        one bool per fig, saying if its legend has to be shown or not. Avoid duplicates legend

    """
    assert cols * rows >= len(list_of_figs)
    assert len(show_legend) == len(list_of_figs)

    subplot_titles = [fig.layout.title for fig in list_of_figs]
    subplots = tls.make_subplots(rows=rows, cols=cols,
                                 subplot_titles=subplot_titles)
    for x, fig in enumerate(list_of_figs):
        c = (x % cols + 1)
        r = int(x / cols) + 1
        for trace in fig.data:
            trace['showlegend'] = show_legend[x]
            subplots.append_trace(trace, r, c)
    # subplots['layout'].update(fig.layout)
    subplots['layout'].update(height=height, width=width)
    x_range, y_range = get_common_range(list_of_figs)
    if same_x_range:
        for i in range(len(list_of_figs)):
            subplots['layout'][f'xaxis{i+1}'].update(range=x_range)
    if same_y_range:
        for i in range(len(list_of_figs)):
            subplots['layout'][f'yaxis{i+1}'].update(range=y_range)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path




def show_plotly(fig, show=False):
    """
        Display a plotly figure
        Parameters:
        -----------
        fig: Plotly fig
        shwo: bool
            True is you want to display the plot or not
    """
    if show:
        offline.init_notebook_mode(connected=False)
        offline.iplot(fig, show_link=False)


def save_plotly(fig, output_dir, name):
    """
    Save a plotly figure localy
    Parameteters;
    -------------
    fig: plotly fig
        figure to be savec
    output_dir: str
        path to the folder
    name: str
        name of the plot (without extension)
        if name is None, the fig is not saved
    Returns:
    --------
    path of the saved figure
    """
    if name is None:
        return None
    ## save to html and then convert to png
    file_path = os.path.join(output_dir, name)
    html_file_path = f"{file_path}.html"
    png_file_path = f"{file_path}.png"

    offline.plot(fig, filename=file_path, show_link=False)
    html_to_png(html_file_path, png_file_path)
    return png_file_path


def html_to_png(html_file_path, png_file_path):
    option = {
        "format": "png",
        "xvfb": "",
        "quality": 100,
        "quiet": ''
    }
    subprocess.call(
        [f'xvfb-run wkhtmltoimage {html_file_path} {png_file_path}'],
        shell=True)
