import os
import pandas as pd
import numpy as np
import subprocess
import plotly.graph_objs as go
import plotly.tools as tls
import plotly.offline as offline
import plotly.graph_objs as go

KEY_COLOR = 'rgb(0,191,255)'
OTHER_COLOR = 'rgba(58, 71, 80, 0.6)'  # dark_grey
RGB_COLOR = 'rgb(0,191,255)'
WIDTH = 1000
HEIGHT = 600


def set_axis(xaxis, xlab, yaxis, ylab):
    if not xaxis:
        xaxis = dict(title=xlab)
    if not yaxis:
        yaxis = dict(title=ylab, showgrid=False)
    return xaxis, yaxis


def set_colors(key_name, s_names):
    """
    key_name: str or list of str
    """
    if key_name:
        key_name = key_name if isinstance(key_name, list) else [
            key_name]  # to allow you to give 1 or multiple key name
        assert all([True if x in s_names else False for x in key_name])
        condlist = [~s_names.isin(key_name)]
        choicelist = [OTHER_COLOR]
        return np.select(condlist, choicelist, default=KEY_COLOR)
    else:
        return KEY_COLOR


def plotter(xaxis, xlab, yaxis, ylab, title, width, height, data, output_dir,
            name, show):
    xaxis, yaxis = set_axis(xaxis, xlab, yaxis, ylab)
    layout = go.Layout(title=title,
                       width=width,
                       height=height,
                       xaxis=xaxis,
                       yaxis=yaxis)
    fig = go.Figure(data=data, layout=layout)

    file_path = save_plotly(fig, output_dir, name)
    show_plotly(fig, show)
    return file_path


# ==================
# All plot function
# ==================
def scatter_plot(data, x_feature=None, y_feature=None, title='', xlab="",
                 ylab="", key_name="",
                 name=None, output_dir="figures", show=True,
                 xaxis=None, yaxis=None, width=WIDTH, height=HEIGHT):
    """
    Save a scatter plot in png format using plotly module

    Parameters:
    -----------
    series: pandas series
        series value to plot
    title: str
        title of the plot
    xlab: str
        label for the x axis
    ylab: str
        label for the y axis
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved
    """
    if isinstance(data, pd.Series):
        s_names = data.index
        s_values = data.values
    elif isinstance(data, pd.DataFrame):
        s_names = data[x_feature]
        s_values = data[y_feature]

    nval = len(s_values)
    assert nval > 0

    rgb_colors = set_colors(key_name, s_names)
    data = [{'type': 'scatter',
             'marker': go.Marker(color=rgb_colors),
             'mode': 'markers',
             'x': s_names,
             'y': s_values
             }]
    return plotter(xaxis, xlab, yaxis, ylab, title, width, height, data,
                   output_dir, name, show)


def line_plot(df, x_feature, y_feature, by_feature=None, title=None, xlab=None,
              ylab=None, key_name=None,
              name=None, output_dir="figures", show=True,
              xaxis=None, yaxis=None, width=WIDTH, height=HEIGHT):
    """
    Save a line plot in png format using plotly module

    Parameters:
    -----------
    series: pandas dataframe
        series value to plot
    title: str
        title of the plot
    xlab: str
        label for the x axis
    ylab: str
        label for the y axis
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved
    """
    assert isinstance(df, pd.DataFrame), "df is not a pandas DataFrame"
    assert len(df) > 0, "Dataframe with zero records"
    cols = df.columns

    col2have = [x_feature, y_feature]
    if by_feature:
        col2have.append(by_feature)
    assert all([True if x in cols else False for x in col2have])

    assert nval > 0

    rgb_colors = set_colors(key_name, s_names)
    data = [{'type': 'scatter',
             'marker': go.Marker(color=rgb_colors),
             'mode': 'line',
             'x': s_names,
             'y': s_values
             }]
    return plotter(xaxis, xlab, yaxis, ylab, title, width, height, data,
                   output_dir, name, show)


def bar_chart(series, title=None, xlab=None, ylab=None, key_name=None,
              name=None,
              sort_x_ascending=True, display_values=True,
              output_dir="figures", show=True, xaxis=None, yaxis=None,
              width=1000, height=600):
    """
    Save a bar plot in png format using plotly module

    Parameters:
    -----------
    series: pandas series
        raw series. The value counts is performed in the function
    title: str
        title of the plot
    xlab: str
        label for the x axis
    ylab: str
        label for the y axis
    key_names: str or list of str
        value(s) of the series to be plot in blue (in str). Either one element or a list of elements
        !!! For boolean value, give a string "True" or "False" and not a boolean value
        If None: all bar will be in blue.
    sort_x_ascending: Bool
        sort the bar in ascending order if True and descending if False.
    display_values: Bool
        Display the value (in percentage) at the top of each bar.
    name: str
        name of the plot
    output_dir: str
        path of the directery where the plot will be saved.
    """

    assert isinstance(series, pd.Series), "series is not a pandas serie"

    value_counts = series.value_counts().sort_index(ascending=sort_x_ascending)
    labels = series.value_counts(normalize=True).mul(100).sort_index(
        ascending=sort_x_ascending)
    s_names = value_counts.index
    s_names = s_names.astype(str)

    s_values = value_counts
    nval = len(s_values)

    assert nval > 0

    rgb_colors = set_colors(key_name, s_names)
    data = [{'type': 'bar',
             'marker': go.Marker(color=rgb_colors),
             'x': s_names,
             'y': s_values,
             'text': [f"{label}%" for label in
                      labels.round(2)] if display_values else None,
             'textposition': 'auto',
             }]

    return plotter(xaxis, xlab, yaxis, ylab, title, width, height, data,
                   output_dir, name, show)


# ==========================
# DISPLAY AND SAVE PLOT
# ==========================
def show_plotly(fig, show=False):
    """
        Display a plotly figure
        Parameters:
        -----------
        fig: Plotly fig
        shwo: bool
            True is you want to display the plot or not
    """
    if show:
        offline.init_notebook_mode(connected=False)
        offline.iplot(fig, show_link=False)


def save_plotly(fig, output_dir, name):
    """
    Save a plotly figure localy
    Parameteters;
    -------------
    fig: plotly fig
        figure to be savec
    output_dir: str
        path to the folder
    name: str
        name of the plot (without extension)
        if name is None, the fig is not saved
    Returns:
    --------
    path of the saved figure
    """
    if name is None:
        return None
    ## save to html and then convert to png
    file_path = os.path.join(output_dir, name)
    html_file_path = f"{file_path}.html"
    png_file_path = f"{file_path}.png"

    offline.plot(fig, filename=file_path, show_link=False)
    html_to_png(html_file_path, png_file_path)
    return png_file_path


def html_to_png(html_file_path, png_file_path):
    option = {
        "format": "png",
        "xvfb": "",
        "quality": 100,
        "quiet": ''
    }
    subprocess.call(
        [f'xvfb-run wkhtmltoimage {html_file_path} {png_file_path}'],
        shell=True)