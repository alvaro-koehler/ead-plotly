# Plot Sizing
WIDTH = 1000
HEIGHT = 600

# Storing the Plot
OUTPUT_DIR = "figures"
PLOT_DEFAULT_NAME = 'plot'
# Coloring
HIGHLIGHT_COLOR = 'rgb(0,191,255)'
BASE_COLOR = 'rgba(58, 71, 80, 0.6)'  # dark_grey

COLORS = [
    'rgb(0,191,255)',  # "deepskyblue"
    'rgb(169, 169, 169)',  # "dark_grey"
    'rgb(75,0,130)',  # "indigo"
    'rgb(255, 195, 0)',  # "orange"
]

# Plot configurations
DOT_SIZE = 2
